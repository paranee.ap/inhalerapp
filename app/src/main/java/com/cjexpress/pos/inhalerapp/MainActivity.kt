package com.cjexpress.pos.inhalerapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability

class MainActivity : AppCompatActivity() {


    var tvTitle: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        tvTitle = findViewById(R.id.tvTitle)

        appUpdateManager = AppUpdateManagerFactory.create(this)
        initAutoUpdate()
    }

    // Creates instance of the manager.
    var appUpdateManager: AppUpdateManager? = null//= AppUpdateManagerFactory.create(this)

    private fun initAutoUpdate() {

        applyString("initAutoUpdate")
        //Track for FlexibleUpdate: background download progress
        appUpdateManager!!.registerListener({ state ->
            // Show module progress, log state, or install the update.
            when (state.installStatus()) {
                InstallStatus.DOWNLOADED -> {
                    popupSnackbarForCompleteUpdate(appUpdateManager!!)
                    applyString("\nInstallStatus.DOWNLOADED")

                }
                InstallStatus.CANCELED -> {
                    applyString("\nInstallStatus.CANCELED")
                }
                InstallStatus.DOWNLOADING -> {
                    applyString("\nInstallStatus.DOWNLOADING")
                }
                InstallStatus.FAILED -> {
                    applyString("\nInstallStatus.FAILED")
                }
                InstallStatus.INSTALLED -> {
                    applyString("\nInstallStatus.INSTALLED")
                }
                InstallStatus.INSTALLING -> {
                    applyString("\nInstallStatus.INSTALLING")
                }
                InstallStatus.PENDING -> {
                    applyString("\nInstallStatus.PENDING")
                }
                InstallStatus.REQUIRES_UI_INTENT -> {
                    applyString("\nInstallStatus.REQUIRES_UI_INTENT")
                }
                InstallStatus.UNKNOWN -> {
                    applyString("\nInstallStatus.UNKNOWN")
                }
            }
        })

        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager!!.appUpdateInfo

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            when (appUpdateInfo.updateAvailability()) {
                UpdateAvailability.UPDATE_AVAILABLE -> {

                    // Request the update.
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                        appUpdateManager!!.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            AppUpdateType.IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            MY_REQUEST_CODE
                        )
                        applyString("\nAppUpdateType.UPDATE_AVAILABLE startUpdateFlowForResult: AppUpdateType.IMMEDIATE")

                    } else if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                        appUpdateManager!!.startUpdateFlowForResult(
                            appUpdateInfo,
                            AppUpdateType.FLEXIBLE,
                            this,
                            MY_REQUEST_CODE
                        )

                        applyString("\nAppUpdateType.UPDATE_AVAILABLE startUpdateFlowForResult: AppUpdateType.FLEXIBLE")
                    }
                }
                UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS -> {
                    applyString("\nAppUpdateType.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS")
                }
                UpdateAvailability.UNKNOWN -> {
                    applyString("\nAppUpdateType.UNKNOWN")
                }
                UpdateAvailability.UPDATE_NOT_AVAILABLE -> {
                    applyString("\nAppUpdateType.UPDATE_NOT_AVAILABLE")
                }

            }
        }
    }

    /* Displays the snackbar notification and call to action. */
    fun popupSnackbarForCompleteUpdate(appUpdateManager: AppUpdateManager) {
        Toast.makeText(this, "update Complete!!", Toast.LENGTH_SHORT).show()

//        When you call appUpdateManager.completeUpdate() in the foreground,
//        the platform displays a full-screen UI which restart the app in the background.
//        After the platform installs the update, the app restarts into its main activity.

//        If you instead call appUpdateManager.completeUpdate() in the background,
//        the update is installed silently without obscuring the device UI.
        appUpdateManager.completeUpdate()
    }

    val MY_REQUEST_CODE = 321
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE) {
             if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Activity.RESULT_OK", Toast.LENGTH_SHORT).show()

                applyString("\nActivity.RESULT_OK | REQ Code ${MY_REQUEST_CODE}")
            } else if (resultCode == ActivityResult.RESULT_IN_APP_UPDATE_FAILED) {
                Toast.makeText(
                    this,
                    "ActivityResult.RESULT_IN_APP_UPDATE_FAILED",
                    Toast.LENGTH_SHORT
                ).show()
                applyString("\nActivity.RESULT_IN_APP_UPDATE_FAILED | REQ Code ${MY_REQUEST_CODE}")
            }else{
                 if (resultCode != RESULT_OK) {
                     Log.d("%%%", "Update flow failed! Result code: $resultCode")
                     // If the update is cancelled or fails,
                     // you can request to start the update again.
                     applyString("\nActivity Result ${resultCode} | REQ Code ${MY_REQUEST_CODE}")
                 }
             }
        }
    }

    override fun onResume() {
        super.onResume()
        appUpdateManager!!
            .appUpdateInfo
            .addOnSuccessListener { appUpdateInfo ->
                // If the update is downloaded but not installed,
                // notify the user to complete the update.
                if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                    popupSnackbarForCompleteUpdate(appUpdateManager!!)
                    applyString("\nOnResume UpdateAvailability.DOWNLOADED")
                }

                if (appUpdateInfo.updateAvailability()
                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                ) {
                    // If an in-app update is already running, resume the update.
                    appUpdateManager!!.startUpdateFlowForResult(
                        appUpdateInfo,
                        AppUpdateType.IMMEDIATE,
                        this,
                        MY_REQUEST_CODE
                    )

                    applyString("\nOnResume UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS startUpdateFlowForResult IMMEDIATE")
                }
            }
    }
    
    fun applyString(str:String){
        tvTitle!!.text = tvTitle!!.text.toString().plus(str)
    }
}
